#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <direct.h>
#include "FoxImguiApi.h"

void on_render()
{
    if (ImGui::Begin("test"))
    {
        ImGui::Columns(2);
        if (ImGui::Button("test button"))
        {

        }
        ImGui::NextColumn();
        ImGui::Text("Fox");
        ImGui::End();
    }
}

int main() 
{
    FoxImguiApi api("FoxTestWindow", 700, 300, on_render);

    while (api.isAlive())
    {
        api.render();
    }
}